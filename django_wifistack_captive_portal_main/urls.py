from django.urls import path

from . import views

urlpatterns = [
    path("", views.home),
    path("success/", views.success),
    path("wifi-node", views.success),
    path("request.gif", views.request_pixel),
    path("data-privacy/", views.static_data_privacy, name="data-privacy"),
    path("disclaimer/", views.static_disclaimer, name="disclaimer"),
    path("imprint/", views.static_imprint, name="imprint"),
    # FIX APPLE CAPTIVE PORTAL DETECTION
    path("success.html", views.home),
    path("detect.html", views.home),
    path("hotspot-detect.html", views.home),
]
