from django.contrib.sites.models import Site
from django.db import models
from django.utils import timezone


class WifiCaptivePortalSiteConf(models.Model):
    site = models.OneToOneField(Site, models.CASCADE)
    wifi4eu_net_ident = models.CharField(max_length=250)
    wifi4eu_lang = models.CharField(max_length=2)
    portal_mode = models.CharField(max_length=30, default="passthrough")
    portal_passthrough_url = models.URLField(
        max_length=250, default="https://example.com/"
    )

    class Meta:
        db_table = "wifi_captive_portal_site_conf"


class WifiCaptivePortalMetadataCache(models.Model):
    ident_key = models.CharField(max_length=200, unique=True)
    meta_data = models.JSONField()
    last_updated = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        self.last_updated = timezone.now()
        super(WifiCaptivePortalMetadataCache, self).save(*args, **kwargs)

    class Meta:
        db_table = "wifi_captive_portal_md_cache"
