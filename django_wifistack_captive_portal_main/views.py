import base64
import subprocess
from ipaddress import IPv4Address

import requests
from django.conf import settings
from django.contrib.sites.models import Site
from django.http import HttpResponse
from django.shortcuts import redirect
from django.template import loader
from django.views.decorators.clickjacking import xframe_options_exempt

from django_wifistack_captive_portal_main.django_wifistack_captive_portal_main.func import (
    get_client_ip,
)
from django_wifistack_captive_portal_main.django_wifistack_captive_portal_main.models import (
    WifiCaptivePortalSiteConf,
    WifiCaptivePortalMetadataCache,
)


def view_404(request, exception=None):
    # make a redirect to homepage
    return redirect("/")


def get_meta_data():
    try:
        meta_data = requests.get(settings.CDSTACK_META_API_URL, timeout=10).json()
    except:
        meta_data = WifiCaptivePortalMetadataCache.objects.get(
            ident_key="meta_data"
        ).meta_data
    else:
        WifiCaptivePortalMetadataCache.objects.update_or_create(
            ident_key="meta_data",
            meta_data=meta_data,
        )

    return meta_data


def enable_access(request):
    # Enable access
    client_ip = get_client_ip(request)

    if isinstance(client_ip, IPv4Address):
        shorewall_add = subprocess.call(
            "/usr/bin/sudo /usr/sbin/shorewall add cpalw {}".format(str(client_ip)),
            shell=True,
        )
    else:
        shorewall_add = subprocess.call(
            "/usr/bin/sudo /usr/sbin/shorewall6 add 6_cpalw {}".format(str(client_ip)),
            shell=True,
        )

    return


def home(request):
    current_site = Site.objects.get_current(request)
    current_site_config = WifiCaptivePortalSiteConf.objects.get(site=current_site)

    template_opts = dict(get_meta_data())

    if current_site_config.portal_mode == "passthrough":
        return success(request)

    template_opts["wifi4eu_net_ident"] = current_site_config.wifi4eu_net_ident
    template_opts["wifi4eu_lang"] = current_site_config.wifi4eu_lang

    template = loader.get_template("django_wifistack_captive_portal_main/home.html")

    return HttpResponse(template.render(template_opts, request))


@xframe_options_exempt
def success(request):
    current_site = Site.objects.get_current(request)
    current_site_config = WifiCaptivePortalSiteConf.objects.get(site=current_site)

    template_opts = dict(get_meta_data())
    template_opts["wifi4eu_net_ident"] = current_site_config.wifi4eu_net_ident
    template_opts["wifi4eu_lang"] = current_site_config.wifi4eu_lang
    template_opts["portal_mode"] = current_site_config.portal_mode
    template_opts["portal_passthrough_url"] = current_site_config.portal_passthrough_url

    hostname = current_site.domain
    hostname_ipv4_parts = hostname.split(".")
    hostname_ipv6_parts = hostname.split(".")

    hostname_ipv4_parts[0] = hostname_ipv4_parts[0] + "-ipv4"
    hostname_ipv6_parts[0] = hostname_ipv6_parts[0] + "-ipv6"

    hostname_ipv4 = ".".join(hostname_ipv4_parts)
    hostname_ipv6 = ".".join(hostname_ipv6_parts)

    template_opts["hostname_ipv4"] = hostname_ipv4
    template_opts["hostname_ipv6"] = hostname_ipv6

    enable_access(request)

    template = loader.get_template("django_wifistack_captive_portal_main/success.html")

    return HttpResponse(template.render(template_opts, request))


def request_pixel(request):
    PIXEL_GIF_DATA = base64.b64decode(
        b"R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
    )

    enable_access(request)

    return HttpResponse(PIXEL_GIF_DATA, content_type="image/gif")


def static_data_privacy(request):
    template_opts = dict(get_meta_data())

    template = loader.get_template(
        "django_wifistack_captive_portal_main/data_privacy.html"
    )

    return HttpResponse(template.render(template_opts, request))


def static_disclaimer(request):
    template_opts = dict(get_meta_data())

    template = loader.get_template(
        "django_wifistack_captive_portal_main/disclaimer.html"
    )

    return HttpResponse(template.render(template_opts, request))


def static_imprint(request):
    template_opts = dict(get_meta_data())

    template = loader.get_template("django_wifistack_captive_portal_main/imprint.html")

    return HttpResponse(template.render(template_opts, request))


def view_404(request, exception=None):
    # make a redirect to homepage
    return redirect("/")
